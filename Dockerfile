FROM golang:latest

ENV GO111MODULE=on
ENV WORKDIR "/app"
WORKDIR $WORKDIR

COPY . $WORKDIR
COPY .env.docker .env

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build $WORKDIR/main.go

CMD ./main
